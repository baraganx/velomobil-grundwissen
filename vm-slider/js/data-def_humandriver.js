var dataDefHumandriver = {
	'minlengthdriver'        : { type : 'range' , unit : 'cm' },
	'maxlengthdriver'        : { type : 'range' , unit : 'cm' },
	'minxseam'               : { type : 'range' , unit : 'cm' },
	'maxxseam'               : { type : 'range' , unit : 'cm' },
	'maxshoulderwidth'       : { type : 'range' , unit : 'cm' , step : .5 },
	'maxshoesize'            : { type : 'range' },
	'mindevelopment'         : { type : 'range' , unit : 'cm' },
	'maxdevelopment'         : { type : 'range' , unit : 'cm' },
	'openchain'              : { type : 'radio' , values : ['open', 'covered']},
};
