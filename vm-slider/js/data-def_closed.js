var dataDefClosed = {
	'driversopeningwidth'    : { type : 'range' , unit : 'cm' },
	'driversopeninglength'   : { type : 'range' , unit : 'cm' },
	'hood'                   : { type : 'checkbox' , values : ['removablehood' , 'foldinghood' , 'foamcover' , 'tarp']},
	'hoodtransport'          : { type : 'checkbox' , values : ['hoodyes', 'hoodno' , 'foamcoveryes', 'foamcoverno']},
}
