var dataDefSteerable = {
	'steering'               : { type : 'checkbox' , values : ['tiller', 'sidestick']},
	'turningcircle28mm'      : { type : 'range' , unit : 'm' , step : .5 },
	'turningcircle40mm'      : { type : 'range' , unit : 'm' , step : .5 },
};
