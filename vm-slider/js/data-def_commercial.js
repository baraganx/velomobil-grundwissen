var dataDefCommercial = {
	'pricefrom'              : { type : 'range' , unit : '€' },
	'builtfrom'              : { type : 'range' , enrich : function(vmData) { if (typeof vmData.builtfrom !== 'undefined' && typeof vmData.builtuntil === 'undefined' && vmData.buyable !== 'no') { vmData.buyable = 'yes' } else if (typeof vmData.builtuntil !== 'undefined' || vmData.buyable !== 'yes') { vmData.buyable = 'no' } } },
	'builtuntil'             : { type : 'range' },
	'buyable'                : { type : 'radio' , values : ['yes' ]},
	'website'                : { type : 'link' },
};
