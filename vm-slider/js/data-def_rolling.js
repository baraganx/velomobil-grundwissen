var dataDefRolling = {
// dimensions
	'numberwheel'            : { type : 'range' , step : 1 },
	'groundclearance'        : { type : 'range' , unit : 'cm' },
	'trackwidth'             : { type : 'range' , unit : 'cm' },
	'wheelbase'              : { type : 'range' , unit : 'cm' },
	'maxfrontwheelwidth'     : { type : 'range' , unit : 'mm' },
	'maxrearwheeldiameter'   : { type : 'range' , unit : 'mm' },
	'frontwheels'            : { type : 'radio' , values : ['open', 'covered']},
	'maxthighwidth'          : { type : 'range' , unit : 'cm' },
	'motorizable'            : { type : 'checkbox' , values : ['hub', 'bottombracket', 'intermediate']},
	'maintenance'            : { type : 'checkbox' , values : ['maintenanchole', 'removablefront', 'removableend', 'openend']},
	'comfortablespeed'       : { type : 'radio' , values : ['low', 'medium', 'high', 'speed']},
	'overallcharacter'       : { type : 'checkbox' , values : ['city', 'travel', 'race']},
	'sidewindsensitivity'    : { type : 'radio' , values : ['high', 'medium', 'low']},
	'curvestability'         : { type : 'radio' , values : ['low', 'medium', 'high']},
};
