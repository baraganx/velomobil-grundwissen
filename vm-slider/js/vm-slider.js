$( function() {

	// UTILITY functions
	function getNode(n, v) {
		n = document.createElementNS("http://www.w3.org/2000/svg", n);
		for (let p in v)
			n.setAttributeNS(null, p.replace(/[A-Z]/g, function(m, p, o, s) { return "-" + m.toLowerCase(); }), v[p]);
		return n;
	}
	function download(filename, text) {
		let pom = document.createElement('a');
		pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
		pom.setAttribute('download', filename);
		pom.click();
	}


	function createGraph(cat, key) {
//		let gr = getNode('polyline', { stroke: 'orange', strokeWidth: 5, fill: 'pink', points: '50 160 55 180 70 180 60 190 65 205 50 195 35 205 40 190 30 180 45 180' });
//		svg.appendChild(gr);
//		var r = getNode('rect', { x: 10, y: 10, width: 100, height: 20, fill:'#ff00ff' });
//		svg.appendChild(r);
//		var r = getNode('rect', { x: 20, y: 40, width: 100, height: 40, rx: 8, ry: 8, fill: 'pink', stroke:'purple', strokeWidth:7 });
//		svg.appendChild(r);
		document.getElementById('').appendChild(svg);
	}
	function createCheckbox(def) {
		$( '#'+def+'Area' ).append('<div id="'+def+'Boxes" class="property-boxes"></div>');
		dataDef[def]['values'].forEach(function(key, index) {
			const checkId = def+'-'+key;
			document.documentElement.style.setProperty('--txt-'+def+'-'+key, '"'+key+'"');
			rules += '.txt-' + def + '-' + key + ':after {content:var(--txt-' + def + '-' + key + ');} ';
			$( '#'+def+'Boxes' ).append('<input type="checkbox" id="'+checkId+'" class="'+def+'Check"><label for="'+checkId+'" class="property-box text txt-'+def+'-'+key+'"></label>');
			$( '#'+checkId ).change(function () {
				if (
					dataDef[def]['type'] == 'radio'
					&& this.checked
				) {
					$( '#'+def+'Boxes' ).find('input').prop('checked', false);
					this.checked = true;
				}
				if ($('.'+def+'Check:checked').length == 0) {
					dataDef[def]['changed'] = false;
				} else {
					dataDef[def]['changed'] = true;
				}
				evaluate();
			});
		});
	}

	function createSlider(def) {
		const stepdata = (dataDef[def]['step'] !== null && dataDef[def]['step'] !== undefined) ? dataDef[def]['step'] : 0.1;


//		console.log(dataDef[def]);
		// No data
		if (dataDef[def]['min'] == 9999 && dataDef[def]['max'] == 0) {
			$( '#'+def+'Area' ).append('<div id="'+def+'Filter" class="property-filter"></div>');
			$( '#'+def+'Filter' ).append('<div id="'+def+'InfoBox" class="property-info txt-notknown"></div>')
		}
		// only one data, view as checkbox
		else if (dataDef[def]['min'] == dataDef[def]['max']) {
			$( '#'+def+'Area' ).append('<div id="'+def+'Boxes" class="property-boxes"></div>');

			const key = "onevalue";
			const value = dataDef[def]['min'];
			const checkId = def+'-'+key;

			$( '#'+def+'Boxes' ).append('<input type="number" id="min-'+def+'" min="'+dataDef[def]['min']+'" max="'+dataDef[def]['max']+'" step="any" style="display:none;">');
			$( '#'+def+'Boxes' ).append('<input type="checkbox" id="'+checkId+'" class=""><label for="'+checkId+'" class="property-box text">'+value+((dataDef[def]['unit'] !== undefined) ? ' '+dataDef[def]['unit'] : '')+'</label>');
			$( '#'+checkId ).change(function () {
				if (!dataDef[def]['changed'])
					dataDef[def]['currMin'] = $('#min-'+def).val();
				dataDef[def]['changed'] = !dataDef[def]['changed'];
				evaluate();
			});
		}
		else {
			$( '#'+def+'Area' ).append('<div id="'+def+'Filter" class="property-filter"></div>');
			// Box with numeric informations
			$( '#'+def+'Filter' ).append('<div id="'+def+'InfoBox" class="property-info"><input type="number" id="min-'+def+'" min="'+dataDef[def]['min']+'" max="'+dataDef[def]['max']+'" step="'+stepdata+'"></input>&nbsp;⟷&nbsp;<input type="number" id="max-'+def+'" min="'+dataDef[def]['min']+'" max="'+dataDef[def]['max']+'" step="'+stepdata+'"></input>'+((dataDef[def]['unit'] !== undefined) ? dataDef[def]['unit'] : '')+'</div>');
			$('#min-'+def).on('input change',function() {
				const inputval = $('#min-'+def).val();
				const inputval2 = $('#'+def+'SliderInputB').val();
				$( '#'+def+'SliderInputA').val(inputval);
				dataDef[def]['currMin'] = inputval;
				if (dataDef[def]['min'] == inputval && dataDef[def]['max'] == inputval2) {
					dataDef[def]['changed'] = false;
				} else {
					dataDef[def]['changed'] = true;
				}
				evaluate();
			});
			$('#max-'+def).on('input change',function() {
				const inputval = $('#max-'+def).val();
				const inputval2 = $('#'+def+'SliderInputA').val();
				$( '#'+def+'SliderInputB').val(inputval);
				dataDef[def]['currMax'] = inputval;
				if (dataDef[def]['max'] == inputval && dataDef[def]['min'] == inputval2) {
					dataDef[def]['changed'] = false;
				} else {
					dataDef[def]['changed'] = true;
				}
				evaluate();
			});

			// Sliders
			$( '#'+def+'Filter' ).append('<div id="'+def+'Slider" class="slider-box property-slider"></div>');
			$( '#'+def+'Slider' ).append('<input id="'+def+'SliderInputA" type="range" min="'+dataDef[def]['min']+'" max="'+dataDef[def]['max']+'" value="'+dataDef[def]['min']+'" step="'+stepdata+'" class="slider">');
			$( '#'+def+'Slider' ).append('<input id="'+def+'SliderInputB" type="range" min="'+dataDef[def]['min']+'" max="'+dataDef[def]['max']+'" value="'+dataDef[def]['max']+'" step="'+stepdata+'" class="slider">');

			$('#'+def+'SliderInputA').on('input change',function() {
				const inputval = $('#'+def+'SliderInputA').val();
				const inputval2 = $('#'+def+'SliderInputB').val();

				$( '#min-'+def).val(inputval);
				dataDef[def]['currMin'] = inputval;
				if (dataDef[def]['min'] == inputval && dataDef[def]['max'] == inputval2) {
					dataDef[def]['changed'] = false;
				} else {
					dataDef[def]['changed'] = true;
				}
				evaluate();
			});
			$('#'+def+'SliderInputB').on('input change',function() {
				const inputval = $('#'+def+'SliderInputB').val();;
				const inputval2 = $('#'+def+'SliderInputA').val();

				$( '#max-'+def).val(inputval);
				dataDef[def]['currMax'] = inputval;
				if (dataDef[def]['max'] == inputval && dataDef[def]['min'] == inputval2) {
					dataDef[def]['changed'] = false;
				} else {
					dataDef[def]['changed'] = true;
				}
				evaluate();
			});
			$( '#min-'+def).val(dataDef[def]['min']);
			$( '#max-'+def).val(dataDef[def]['max']);
		}
	}

	function createVmList() {
		$('#result').html('');
		$('#select').html('');
		initCount();
		$('#result').append('<div id="navleft" class="nav"><button id="scrollpageleft"></button><button id="scrolloneleft"></button><input type="text" id="val-scrolloneleft" readonly><input type="text" id="val-scrollpageleft" readonly></div>')
		$( "#scrolloneleft" ).click(function() {
			$('#sliderpageid').val($('#val-scrolloneleft').val());
			evaluate();
		});
		$( "#scrollpageleft" ).click(function() {
			$('#sliderpageid').val($('#val-scrollpageleft').val());
			evaluate();
		});
		document.addEventListener('keyup', function(e) {
			if (e.key === 'ArrowRight' || e.key === 'd') {
				$('#sliderpageid').val($('#val-scrolloneright').val());
				evaluate();
			}
			else if (e.key === 'ArrowLeft' || e.key === 'q') {
				$('#sliderpageid').val($('#val-scrolloneleft').val());
				evaluate();
			}
			else if (e.key === ' ') {
				document.getElementById("view-comparator").checked = !document.getElementById("view-comparator").checked;
				evaluate();
			}
			else if (e.key === 'x') {
				download('vm-data2.js','let vmData=' + JSON.stringify(vmData) + ';');	
			}
// TODO doesn't work for page
//			else if (e.ctrlKey && e.key === 'ArrowRight') {
//				$('#sliderpageid').val($('#val-scrollpageright').val());
//				evaluate();
//			}
//			else if (e.ctrlKey && e.key === 'ArrowLeft') {
//				$('#sliderpageid').val($('#val-scrollpageleft').val());
//				evaluate();
//			}
		}, false);

		$( "#navleft" ).click(function() {
			if (prevcursor == 0)
				$('#sliderpagerange').val((parseInt($('#sliderpagerange').val()) - 1) + '');
			else
				$('#sliderpagerange').val(prevcursor + '');
			$('#sliderpagenum').val($('#sliderpagerange').val());
			evaluate();
		});

//		$.each(vmData, function(key, vm) {
		let i = 0;
		for (const [key, vm] of Object.entries(vmData)) {
//		Object.entries(vmData).forEach(([key, vm]) => {
//			console.log(i);

			// initial value starting application !
			if (i == 0) {
				let initialkey = searchParams.get('key')
				if (!initialkey) initialkey = key;
				$('#sliderpageid').val(initialkey);
			}

			vmstate[key] = { 'selected' : true, filter : []};
			$('#result').append('<details open class="checkicon" id="'+key+'"><summary><label for="view-detail" class="labelname" title="'+key+'">'+vm.name+'</label></summary>'+vmDetails(key, vm)+'</details>');
			$('#select').append('<div class="" id="sel-'+key+'"><div><label class="labelname"><span class="numname">'+(i+1)+'</span>'+vm.name+'</label></div></div>');
			$('#sel-'+key).click(function() {
				$('#sliderpageid').val(key);
				evaluate();
			});
//			Event listener on details (opening / closing)
			$('#'+key).on('toggle', function() {
				if ($('#'+key).prop('open')) {
//					vmstate[key].push('selected');
					vmstate[key]['selected'] = true;

//				console.log(vmstate);
				}
				else {
					vmstate[key]['selected'] = false;
				}
				evaluate();
			});
			i++;
		};
		$('#result').append('<div id="navright" class="nav"><button id="scrollpageright"></button><button id="scrolloneright"></button><input type="text" id="val-scrolloneright" readonly><input type="text" id="val-scrollpageright" readonly></div>')
		$( "#scrolloneright" ).click(function() {
			$('#sliderpageid').val($('#val-scrolloneright').val());
			evaluate();
		});
		$( "#scrollpageright" ).click(function() {
			$('#sliderpageid').val($('#val-scrollpageright').val());
			evaluate();
		});
		updateCount();
		$('#sliderpagenum').val($('#sliderpagerange').val());
	}
	
	function initAppstateEvent() {
		$( "#reload" ).click(function() {
			$("#choice").empty();
			$("#result").empty();
	//		$("#result").removeClass("imageattenuate");
	//		$("#result").addClass("imageattenuate");
			build();
		});
		$.each(appstate, function(key, val) {
//console.log(key);
//console.log(appstate);
			$('#'+key).change(function () {
//console.log("before:");
//console.log(appstate);
				if($('#'+key).is(':checked'))
					appstate[key] = false;
				else
					appstate[key] = true;
//console.log("after:");
//				$(':root').css('--maxcolumns','20');
//				console.log(appstate);
				evaluate();
			})
		});





	}
	function initCount() {
		validcount = 0;
		uncertaincount = 0;
		invalidcount = 0;
//		totalcount = 0;
		pagecount = 0;
		countselect = 0; //TODO
	}
	function updateCount() {
		$('#selectcount').empty();
		if (validcount == 0)
			$('#selectcount').append('<span class="text txt-select">0 / '+(invalidcount)+' </span>');
		else if (validcount == 1)
			$('#selectcount').append('<span class="text txt-select">1 / '+(validcount+invalidcount)+' </span>');
		else if (validcount > 1)
			$('#selectcount').append('<span class="text txt-select-2">'+validcount+' / '+(validcount+invalidcount)+' </span>');
		

		if (uncertaincount == 1)
			$('#selectcount').append(' [<span class="text txt-data">'+uncertaincount+' </span> <span class="txt-notknown"></span>]');
		else if (uncertaincount > 1)
			$('#selectcount').append(' [<span class="text txt-data-2">'+uncertaincount+' </span> <span class="txt-notknown-2"></span>]');
		$('#selectcount').append(', total '+totalcount);

	}
	function loadLang(lang) {
		if (lang) {
			$('<link>').appendTo('head')
			.attr({
				type: 'text/css', 
				rel: 'stylesheet',
				href: 'css/vm-slider_'+lang+'.css'
			});
			$('#lang'+lang).addClass('langactive');
		}
	}

	function vmDetails(vmKey, vm) {
		let html = '<dl class="itemboxes">';
		let html2 = '';
		validcount +=1 ;
		$.each(dataDef, function(key, definition) {
			let value = '';
			if (vm.hasOwnProperty(key)) {
				let tooltip = '';
				if (definition.type == 'imageurl') {
					html2 += '<div class="prop-'+key+'" style="background-image:url(\''+vm[key]+'\');"><div class="gradient-image"></div></div>';
				}
				switch (definition.type) {
					case 'checkbox':
						/* checkbox data does not need to be an object if it has just one value */
						if (typeof vm[key] !== 'object') {
							let makeObject = vm[key];
							vm[key] = [ makeObject ];
						}
						
						vm[key].forEach(function(val, index) {
							value += '<label for="'+key+'-'+val+'" class="property-box txt-'+key+'-'+val+'"></label>';
						});
//						tooltip = 'title="'+value+'"';
						break;
					case 'radio':
						value = vm[key];
						break;
					case 'range':
						value = vm[key];
						if (definition.hasOwnProperty('unit')) {
							value += ' ' + definition.unit;
						}
						break;
					case 'info':
						value = vm[key];
						break;
					case 'link':
						value = '<a href="'+vm[key]['url']+'" target="_blank">'+vm[key]['label']+'</a>';
						break;
					case 'imageurl':
						value = '<img src="'+vm[key]+'">';
						break;
				}

				if (sources[vmKey].hasOwnProperty(key)) {
					value += ' <a href="'+sources[vmKey][key]+'" target="_blank">*</a>';
				} 
				
				html += '<dt class="prop-'+key+' txt-'+key+'"></dt>';
				if (definition.type == 'checkbox')
					html += '<dd class="prop-'+key+'">'+value+'</dd>';
				else if (definition.type == 'radio')
					html += '<dd class="prop-'+key+'"><label for="'+key+'-'+value+'" class="property-box txt-'+key+'-'+value+'"></label></dd>';
				else
					html += '<dd class="prop-'+key+'">'+value+'</dd>';
			}
			else {
				html += '<dt class="prop-nodata prop-'+key+'"></dt><dd class="prop-nodata prop-'+key+'"></dd>';
			}
		});
		html += '</dl>';
		return html2 + html;
	}

	function evaluate() {
		initCount();
		vmnum = 0;
		vmcursor = [];
		$.each(vmData, function(key, value) {
			const visibility = vmVisibility(key,value);
			$('#'+key).attr( 'class', 'vm-'+visibility);
			$('#sel-'+key).attr( 'class', 'vm-'+visibility);
			vmnum += 1;
			vmVisibilityPage(key, vmnum);
//			totalcount += 1;

		});
		$.each(dataDef, function(key, value) {
			$('#'+key+'Area').removeClass('property-changed');
//			$('#'+key+'InfoBox').removeClass('property-changed');
			if (value.changed) {
				$('#'+key+'Area').addClass('property-changed');
//				$('#'+key+'InfoBox').addClass('property-changed');
			}

// TODO indicateur si modif dans les filtres
//			if (value.changed && !$('#reload').hasClass('property-changed')) {
//				$('#reload').addClass('property-changed');
//			}
//			else if (value.changed && $('#reload').hasClass('property-changed')) {
//				$('#reload').removeClass('property-changed');
//			}
		});
		updateCount();
	}

	function vmVisibility(vmkey,vm) {
		let visibility = true;
		let uncertain = false;
		$.each(dataDef, function(key, def) {
			if (
				def.changed
				&& !vm.hasOwnProperty(key)
			) {
				uncertain = true;
				uncertaincount += 1;
				return;
			}

			if (vm.hasOwnProperty(key)) {
				switch(def.type) {
					case 'range':
						if (def.currMin > vm[key] || def.currMax < vm[key]) {
							visibility = false;
						}
						break;

					case 'radio':
					case 'checkbox':
						let checkVisibility = false;
						let checkedNothing = true;
						def.values.forEach(function(vkey,index) {
							const checkId = key+'-'+vkey;
							if ($('#'+checkId).prop('checked')) {
								checkedNothing = false;
								if (
									$.inArray(vkey, vm[key]) > -1 // checkboxes
									|| vkey == vm[key] //radio
								) {
									checkVisibility = true;
								}
							}
						})
						visibility = visibility && (checkedNothing || checkVisibility);
						break;
				}
			}
		});
		if (!uncertain) {
			if (visibility) {
				vmstate[vmkey]['filter'] = 'valid';
				validcount +=1 ;
			}
			else {
				vmstate[vmkey]['filter'] = 'invalid';
				invalidcount += 1;
			}
		}
		else {
			vmstate[vmkey]['filter'] = 'uncertain';
		}
		return uncertain ? 'uncertain' : ( visibility ? 'valid' : 'invalid' );
	}

	function vmVisibilityPage(key, vmnum) {
//console.log(key);
//		console.log(appstate['view-vm-'+vmstate[key].filter]);
		maxcolumns = parseInt(window.getComputedStyle(document.getElementById('content')).getPropertyValue('--maxcolumns')) || 2;
//		console.log(maxcolumns);
		const cursor = parseInt($('#sliderpagerange').val());
		const cursorkey = $('#sliderpageid').val();
		
		if (key == cursorkey) {
			$('#sliderpagerange').val(vmnum + '');
			$('#sliderpagenum').val(vmnum + '');
			if (!$('#sel-'+key).hasClass('cursor'))
				$('#sel-'+key).addClass('cursor');
			if (typeof vmcursor[0] === 'undefined')
				$('#val-scrolloneleft').val(key);
			else
				$('#val-scrolloneleft').val(vmcursor[0]);

			for (let i = 0; i < maxcolumns ; i++) {
				if (typeof vmcursor[maxcolumns-1-i] === 'undefined')
					$('#val-scrollpageleft').val(key);
				else {
					$('#val-scrollpageleft').val(vmcursor[maxcolumns-1-i]);
					break;
				  }
			}
		}
		if (pagecount == 1) {
			$('#val-scrolloneright').val(key);
		}
		if (pagecount == maxcolumns) {
			$('#val-scrollpageright').val(key);
			pagecount++;
		}
		if (pagecount > 1 && pagecount < maxcolumns) {
			$('#val-scrollpageright').val(key);
		}

		// Visibility of entry
		if (	key == cursorkey ||
			(
			pagecount > 0 &&
			pagecount < maxcolumns &&
			appstate['view-vm-'+vmstate[key].filter]
			)
			) {
			pagecount++;
			if (!$('#'+key).hasClass('visiblepage'))
				$('#'+key).addClass('visiblepage');
			if (!$('#sel-'+key).hasClass('visiblepage'))
				$('#sel-'+key).addClass('visiblepage');
		}
		else
			$('#'+key).removeClass('visiblepage');

		if (appstate['view-vm-'+vmstate[key].filter])
			vmcursor.unshift(key);
	}

	function build() {

		// Retrieve variable names loaded with script in HTML header
		const datadefprefix = 'dataDef' ;
		const datadefvars = Object.keys(window).filter(e => e.startsWith(datadefprefix));

//		console.log(datadefvars);

		datadefvars.forEach(function(name) {
//			console.log(window[name]);
			dataDef = Object.assign(dataDef,window[name]);
			dataDefCat[name.slice(datadefprefix.length)] = window[name];
		});
//		console.log("datadef");
//		console.log(dataDef);

//		console.log("datadefcat");
//		console.log(dataDefCat);

		/* if a value has a source, copy source in object and move value one level up */
		for (let vm in vmData) {
			sources[vm] = {};
			for (attr in vmData[vm]) {
				if (typeof vmData[vm][attr] === 'object' && vmData[vm][attr].hasOwnProperty('source')) {
					sources[vm][attr] = vmData[vm][attr]['source'];
					value = vmData[vm][attr]['value'];
					vmData[vm][attr] = null;
					vmData[vm][attr] = value;
				}
			}
			totalcount += 1;
		}
		
		for (let def in dataDef) {
			for (let vm in vmData) {
				if (dataDef[def].hasOwnProperty('enrich')) {
					dataDef[def].enrich(vmData[vm]);
				}

				switch (dataDef[def]['type']) {
					case 'checkbox':
					case 'radio':
						break;
					case 'range':
						dataDef[def]['min'] = Math.min((dataDef[def]['min'] !== null && dataDef[def]['min'] !== undefined) ? dataDef[def]['min'] : 9999, ((vmData[vm][def] !== null && vmData[vm][def] !== undefined) ? vmData[vm][def] : 9999));
						dataDef[def]['max'] = Math.max((dataDef[def]['max'] !== null && dataDef[def]['max'] !== undefined) ? dataDef[def]['max'] : 0, ((vmData[vm][def] !== null && vmData[vm][def] !== undefined) ? vmData[vm][def] : 0));
						dataDef[def]['currMin'] = dataDef[def]['min'];
						dataDef[def]['currMax'] = dataDef[def]['max'];
						break;
				}
			}
			dataDef[def]['changed'] = false;
		}
		$('#choice').append('<div id="sliderpage" class="slider-box"><input type="number" min="1" max="'+totalcount+'" id="sliderpagenum" readonly><input type="text" id="sliderpageid"><input type="range" min="1" max="'+totalcount+'" value="1" class="slider" id="sliderpagerange"></div>');
		$('#sliderpagerange').on('input change',function() {
//			$('#sliderpagerange').val('20');
//			console.log(parseInt($('#sliderpagerange').val()));
			$('#sliderpagenum').val($('#sliderpagerange').val());
			evaluate();
		});
		$('#sliderpagenum').change(function() {
			$('#sliderpagerange').val($('#sliderpagenum').val());
			evaluate();
		});
		$('#sliderpageid').change(function() {
//			$('#sliderpagerange').val($('#sliderpageid').val());
			evaluate();
		});


		// Add boilerplate class to link --txt-dataDefFields with .txt-dataDefFields
		rules = '';
		let style = document.createElement('style');
		style.id = "classtxtstyle" ; // so you can get and alter/replace/remove later
//		style.type = 'text/css';
//		document.getElementsByTagName('head')[0].appendChild(style);

		for (let cat in dataDefCat) {
//			$('#content').append('<input type="checkbox">'); TODO
			$('#choice').append('<div id="impl-'+cat+'" class="impl"><div class="impl-summary"><label>'+cat+'</label></div><div id="implbox-'+cat+'" class="implbox"></div></div>');
			for (let def in dataDefCat[cat]) {
				let sliderArea = '<div id="'+def+'Area" class="vm-property"><div id="'+def+'Title" class="text txt-'+def+'"></div></div>';
				// add default text for var
				document.documentElement.style.setProperty('--txt-'+def, '"'+def+'"');
				// add default class correspondant with var
				rules += '.txt-' + def + ':after {content:var(--txt-' + def + ');} ';
				
//				if(!(style.sheet||{}).insertRule) 
//					(style.styleSheet || style.sheet).addRule(name, rules);
//				else
//					style.sheet.insertRule(name+"{"+rules+"}",0);

				$('#implbox-'+cat).append(sliderArea);
				switch (dataDef[def]['type']) {
					case 'checkbox':
					case 'radio':
						createCheckbox(def);
						break;
					case 'range':
						createSlider(def);
						break;
				}
			}
		}
		style.innerHTML = rules;
		document.getElementsByTagName("HEAD")[0].appendChild(style) ;

		createVmList();

	}
	let svg = getNode("svg");
	let rules = '';
	let dataDef = {};
	let dataDefCat = {};
	let sources = {};
	let vmstate = {};
	let vmcursor = [];
	let appstate = {'view-comparator':true,
			'view-vm-valid':true,
			'view-vm-invalid':true,
			'view-vm-uncertain':true
	};
	let validcount = 0;
	let invalidcount = 0;
	let uncertaincount = 0;
	let totalcount = 0;
	let nextcursor = 0;
	let prevcursor = 0;
//	console.log($(location).attr('href'));
	const searchParams = new URLSearchParams(window.location.search)
	let lang = searchParams.get('lang')
	const userLang = navigator.language || navigator.userLanguage;
	if (!lang) lang = userLang.substring(0,2);

	// Remove default action for keyboard with left and right arrow
	window.addEventListener("keydown", function(e) {
		if (e.key === 'ArrowLeft' || e.key === 'ArrowRight' || e.key === ' ') { 
			e.preventDefault();
		}
	}, false);

	loadLang(lang);
	initAppstateEvent();

	build();

//	$(':root').css('--maxcolumns','20');
//			document.documentElement.style.setProperty('--totalcolumns', '20');
});
