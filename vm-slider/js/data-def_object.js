var dataDefObject = {
// dimensions
//	'weight'                 : { type : 'range' , unit : 'kg' , step : .5 },
	'image'			 : { type : 'imageurl' },
	'weight'                 : { type : 'range' , unit : 'kg' , step : .1},
	'length'                 : { type : 'range' , unit : 'cm' },
	'width'                  : { type : 'range' , unit : 'cm' },
	'height'                 : { type : 'range' , unit : 'cm' },

};
