# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = python3 `which sphinx-build`
SPHINXPROJ    = Velomobil-Grundwissen
SOURCEDIR     = .
BUILDDIR      = _build

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile images

# generate all images:
# * plot all Matplotlib diagrams
# * create GraphViz diagram
# * before, add timeline to GraphViz diagram:
#   * extract all years (in parentheses) from nodes
#   * save the node names in hash %a with key = year
#   * after the nodes (i.e. when the first "_future" key is encountered and not a comment): create list of years (in array @years)
#   * save the node names that contain "_future" in the same hash %a (key = "future")
#   * => only if there is no node entry for the last year; otherwise: don’t store the future node, and remove it from the line
#   * save all lines of file in array @lines
#   * at the end: output all lines
#   * add "_future" nodes to the last recorded year
#   * output pairs of years where "//// TIMELINE" is written in file
#   * output a list of "_future" nodes with shape="none" directly afterwards
#   * output each year with its nodes and "rank=same" where "//// RANKS" is written in the file
images:
	cd images && \
	python3 ../data/plot_diagrams.py && \
	../data/plot_maps.sh && \
	perl -ale \
	'	/\((\d{4})\)<br/ and push(@{$$a{$$1}}, $$F[0]);'\
	'	!@years and /\w+_future/ and !/\/\/.*_future/ and @years = sort { $$a <=> $$b } grep { !/_future/ } keys %a; '\
	'	/(\w+)\s*->\s*(\w+_future)\W/ and ( !grep { /$$1/ } @{$$a{$$years[-1]}} and push(@{$$a{future}}, $$2) or s/\s*->\s*\w+_future// );'\
	'	push(@lines, $$_);'\
	'END {'\
	'	map {'\
	'		$$_ = join "\n", map { "\t$$years[$$_-1] -> $$years[$$_];" } (1..$$#years);'\
	'		$$_ .= "\n\n\tnode [ shape=\"none\" label=\"\" ];\n\n";'\
	'		$$_ .= join "\n", map { "\t$$_;" } @{$$a{future}};'\
	'	} grep { m!//// TIMELINE$$! } @lines; '\
	'	push(@{$$a{$$years[-1]}}, @{delete $$a{future}});'\
	'	map { $$_ = join "\n", map { "\t{ rank=same; " . join( " ", $$_, @{$$a{$$_}} ) . "; }" } @years } grep { m!//// RANKS$$! } @lines; '\
	'	print for @lines; '\
	'}' ../data/vm_history_de.dot | dot -Tsvg:cairo > vm_history_de.svg

# EPUB: change MathJax to imgmath
epub: Makefile
	$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O) -D extensions=sphinx.ext.imgmath,sphinx.ext.ifconfig,sphinxcontrib.rsvgconverter

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

