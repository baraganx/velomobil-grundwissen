#!/bin/sh

# Region: W, S, E, N
REGION='-R3/45.2/19/55.8+r'

# Map projection: Lambert, around 10E/50N, with parallels 45N and 55N
PROJ='-JL10/50/45/55/16c'



# MAP 1:
# Velomobile dealers in central Europe; source:
# https://www.google.com/maps/d/viewer?hl=de&mid=1Mr5RVj9MMaFSy7xnXUbZmODqEAZDI4n1

FILE='map_velomobile-dealers'
gmt begin "$FILE" pdf


# Plot coastlines, lakes and borders:
# -R: region
# -J: map projection
# -D: resolution (crude, low, intermediate, high, full)
# -A: lakes >= 20 km^2, up to level 2
# -S, -G: colors for water/land
# -N: draw state boundaries
# colors: see https://docs.generic-mapping-tools.org/latest/_images/GMT_RGBchart.png
gmt coast "$REGION" "$PROJ" -Dh -A20/0/2 -Sroyalblue4 -Gdarkgrey -N1


# Extract coordinates and labels from KML file,
# and write a CSV table to a temporary file:
TEMP=$(mktemp)
sed -n -e '
	# if line contains a name tag:
	/<name>/ {
		# extract name
		s!.*<name>\(.*\)</name>.*!\1!;

		# remove CDATA tags, if present
		s/<!\[CDATA\[\(.*\)\]\]>/\1/;

		# shorten names:
		# * remove everything after space and slash or dash
		# * remove everything after comma
		# * remove company type and everything afterwards
		# * combine all Dronten companies in one label
		s! [/-].*\|,.*!!;
		s! \(GmbH\|UG\|Inh\).*!!;
		s/\(Beyss\).*/\1/;
		s/Velomobiel[^ ]*$/Velomobiel, InterCityBike, Alligt/;

		# push on stack, and go to next line
		h;
		b;
	}

	# coordinates: write them tab separated
	s/.*[^0-9.-]\([0-9.-]\+\),\([0-9.-]\+\),0.*/\1\t\2\t/;

	# if substitution did not work, go to next line
	T;

	# append label from stack and remove linebreak
	G;
	s!\n!!g;

	# remove some entries, and print
	/R3\|Intercity\|Gazelle\|[^,][^ ]Alligt\|LEITRA DK/b;
	p;
	' "$(dirname "$0")/Velomobilhändler und -hersteller.kml" | tee "$TEMP"


# Write labels:
# -F: font
#   * +f6p: size 6p
#   * +jML: coordinates are middle/left of label
# -D: offset x/y to coordinates
# -G: background color
gmt text -F+f6p+jML -D0.1c/0c -Glightgrey < "$TEMP"


# Plot circles (afterwards, so they are on top of labels when overlapping):
# -Sc: circle, with diameter
# -G: background color
# -W: plot outline
gmt plot -Sc0.15c -Gred2 -W < "$TEMP"


# Write file
gmt end
rm -f "$TEMP"


# Convert to SVG
pdftocairo -svg "$FILE.pdf"

